import 'dart:js';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            'ธนพัฒน์ สกุลตลากุล อายุ 22 ปี เพศ ชาย',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
    Widget skillSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text('ภาษาที่เขียน:',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          Image.asset(
            'images/js.png',
            width: 50,
            height: 80,
          ),
          Image.asset(
            'images/node.png',
            width: 80,
            height: 100,
          ),
          Image.asset(
            'images/vue.png',
            width: 50,
            height: 50,
          ),
          Image.asset(
            'images/flutter.png',
            width: 80,
            height: 100,
          ),
        ],
      ),
    );
    Widget textSection = Container(
        padding: EdgeInsets.all(32),
        child: Column(
          children: [
            Text('ข้อมูลส่วนตัว',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            Text('อาศัยอยู่ที่: 21/50 อ.ศรีราชา จ.ชลบุรี 20110',
                style: TextStyle(fontSize: 18)),
            Text('ข้อมูลติดต่อ:',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            Text('เบอร์โทรศัพท์: 095-762-8133 ,อีเมล: jay199950@gmail.com',
                style: TextStyle(fontSize: 18)),
          ],
        ));
    Widget educationSection = Container(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text('การศึกษา: ชั้นปีที่4 คณะ เทคโนโลยีสารสนเทศ สาขา วิทยาการคอมพิวเตอร์ ณ มหาวิทยาลัยบูรพา',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
      ],
    ));
    return MaterialApp(
        title: 'My Resume',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('My Resume'),
          ),
          body: ListView(
            padding: const EdgeInsets.only(top: 20),
            children: [
              Image.asset(
                'images/me.jpg',
                width: 600,
                height: 260,
              ),
              titleSection,
              skillSection,
              textSection,
              educationSection
            ],
          ),
        ));
  }
}
